#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "./../header/main.h"


// -------------------------
// Fonction qui va demander à l'utilisateur si il a un fichier perroquet peroq.def
// ou si il faut lui demander de l'écrire dans le terminal
// argument:
//     un pointeur vers perroquet_choix dans main
// retourne:
//     void
void verify_perroquet(char *perroquet_choix){
    do{
        printf("AVEZ-VOUS UN FICHIER PERROQUET? (o/n)\n");
        scanf(" %c", perroquet_choix);
        if (*perroquet_choix != 'o' && *perroquet_choix!= 'n'){
            printf("mauvaise option, mettre o ou n \n");
        }
    }while(*perroquet_choix != 'o' && *perroquet_choix!= 'n');
}

// -------------------------
// Fonction qui permet la mise en forme du programme pour demander 
// à l'utilisateur les actions à faire
// argument:
//     un pointeur vers choix dans main
// retourne:
//     void
void menu(int *choix){

    printf("========== MENU PRINCIPAL ==========\n|\tveuillez choisir une option\n|\n");
    printf("|\t1/ ENCRYPTER UN FICHIER TEXTE\n");
    printf("|\t2/ DECRYPTER UN FICHIER ENCRYPTÉ\n");
    printf("|\t3/ REINITIALISER POUR REFAIRE (ATTENTION À REFAIRE UN SOURCE.TXT)\n");
    printf("|\t4/ AIDE\n");
    printf("|\t0/ SORTIR DU PROGRAMME\n");
    printf("===================================\n");

    // Scanf jusqu'à avoir une valeur bonne
    do{
        printf("option: ");
        scanf(" %d", choix);
        if (*choix > 4 || *choix < 0){
            printf("MAUVAISE VALEUR\n\n");
        }
    }while(*choix > 4 || *choix < 0);

}

void show_help(void){
    printf("\n --- AIDE--- \n\
    Le programme crypt permet d'encrypter un fichier texte contenant des phrases que l'on veut garder secrètes.\
    Pour cela, il va utiliser l'algorithme du \"perroquet\".\
    Il va soustraire le code ASCII de chaque lettre de notre fichier texte que l'on veut encrypter par le code ASCII de chaque lettre d'un mot (ou d'une phrase) appelé(e) \"perroquet\" que l'on donnera soit via un fichier soit directement en ligne de commande.\n\n");

    printf("\
        - Le programme nécessite un fichier source.txt avec des lignes de 1000 caractères maximum\n");
    printf("\
        - Le programme nécessite un perroquet de 1000 caractères max (soit un mot soit une phrase) soit dans un fichier peroq.def soit rentré en ligne de commande en suivant les instructions \n\n");
    printf("\
        - Pour encrypter le fichier source.txt, appuyer sur 1 puis entrée\n");
    printf("\
        - Pour décrypter le fichier encrypté dest.crt, appuyer sur 2 puis entrée\n");
    printf("\
        - Pour refaire ces manoeuvres, appuyer sur 3 pour réinitialiser\n");
    printf("\
        \n\n --- FIN AIDE --- \n\n");
}
