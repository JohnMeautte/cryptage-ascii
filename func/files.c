#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "./../header/main.h"

#define TAILLE_MAX 1000

// -------------------------
// Fonction qui va lire le fichier source.txt en utilisant de l'allocation dynamique
// arguments:
//     le nom du fichier (source.txt)
//     un tableau 2D qui va contenir les lignes du fichier texte
//     un pointeur pour récupérer le nombre de lignes

// retourne:
//     le tableau rempli des lignes
char **read_source(char *filename, char **tab, int *nb_lines){
    FILE *file = NULL;
    int cpt = 1;
    file = fopen(filename, "r");

    // Vérfication bonne ouverture fichier
    if (file == NULL){
        printf("Problème ouverture fichier source\n");
        exit(EXIT_FAILURE);
    }
    // tant qu'on atteint pas la fin du fichier
    while(!feof(file)){

        // on alloue de la mémoire et on stocke les lignes
        tab = realloc(tab, cpt*sizeof(char*));
        tab[cpt-1] = malloc(TAILLE_MAX*sizeof(char));

        fgets(tab[cpt-1], TAILLE_MAX, file);

        // compteur et nombre de lignes
        cpt++;
        (*nb_lines)++;

    }

    // Option pour s'assurer d'avoir une dernière ligne sans rien
    // Augmente la compatibilité avec tous les formats .txt
    if (strnlen(tab[*nb_lines-1], TAILLE_MAX)!=1000){
        // On réalloue un dernière ligne sans rien
        tab = realloc(tab, (*nb_lines+1)*sizeof(char*));
        tab[(*nb_lines)] = malloc(TAILLE_MAX*sizeof(char));
        strcpy(tab[(*nb_lines)], "");
        (*nb_lines)++;
    }


    fclose(file);
    remove(filename);

    return tab;
}

// -------------------------
// Fonction qui va lire le fichier perroquet
// argument:
//     le nom du fichier perroquet (peroq.def)
// retourne:
//     le contenu du fichier perroquet sans de \n
const char *read_perroquet(char *filename){
    FILE *file = NULL; 
    static char per[TAILLE_MAX] = "";
    file = fopen(filename, "r");

    // Si problème ouverture, on quitte
    if (file == NULL){
        printf("Problème ouverture fichier perroquet\n");
        exit(EXIT_FAILURE);
    }

    // récupération perroquet
    fgets(per, TAILLE_MAX, file);
    per[strcspn(per, "\n")] = 0;
    fclose(file);

    return per;

}

// -------------------------
// Fonction qui va libérer la mémoire du tableau contenant les lignes texte
// arguments:
//     le tableau contenant les lignes texte
//     le nombre de lignes

void free_tab(char **tab, int nb_lines){
    for (int i = 0; i < nb_lines; ++i){
            free(tab[i]);   
    }
    free(tab);
}
