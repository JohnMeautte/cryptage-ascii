#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "./../header/main.h"

#define TAILLE_MAX 1000


// -------------------------
// Fonction qui va encrypter le tableau contenant les lignes
// arguments:
//     le tableau avec les lignes
//     le mot (ou phrase) perroquet
//     le nombre de lignes
// retourne:
//     le tableau avec les lignes encryptées
char **encrypt(char **tab, char *perroquet, int nb_lines){

    int indice = 0;

    // On parcourt le tableau contenant les string
    for (int i = 0; i < nb_lines; ++i){
        for (int j = 0; j < strnlen(tab[i], TAILLE_MAX); ++j){
            // On vérifie si le code ASCII ne donnera pas \0 après cryptage
            if (tab[i][j] - perroquet[indice] == 0){
                // Si oui, on met manuellement le code 127 (dernier)
                tab[i][j] -= perroquet[indice];
                tab[i][j] += 127; 
            }
            // Sinon, on soustrait via le perroquet
            else
                tab[i][j] = tab[i][j] - perroquet[indice];
            
            indice++;
            if (indice == strlen(perroquet))
                indice = 0;
         } 
    }
    return tab;
}

// -------------------------
// Fonction qui va créer le fichier texte encrypté à partir du tableau
// contenant les lignes et le perroquet
// arguments:
//     le tableau contenant les lignes texte
//     le mot (ou phrase) perroquet
//     le nombre de lignes
// retourne:
//     void, crée le fichier encrypté dest.crt
void create_encrypt_file(char **tab, char *perroquet, int nb_lines){
    FILE *file = NULL;
    file = fopen("dest.crt", "w+");

    // Vérification bonne ouverture fichier
    if (file == NULL){
        printf("Problème création fichier crypté\n");
        exit(EXIT_FAILURE);
    }

    // On crypte le tableau contenant les lignes
    tab = encrypt(tab, perroquet, nb_lines);

    // On insère dans le fichier dest.crt
    for (int i = 0; i < nb_lines-1; ++i){
        fputs(tab[i], file);
    }

    fclose(file);
}

// -------------------------
// Fonction qui va lire décrypter le fichier encrypté dest.crt
// argument:
//     le mot (ou phrase) perroquet
// retourne:
//     void, crée le fichier décrypté fichier_decrypte.txt
void decrypt(char perroquet){
    int indice = 0;
    char per[TAILLE_MAX] = "";
    char c;

    // file_read: fichier crypté, file_write: fichier décrypté
    FILE *file_write = NULL;
    FILE *file_read = NULL;
    file_write = fopen("fichier_decrypte.txt", "w+");
    file_read = fopen("dest.crt", "r");

    // Vérification bonne ouverture fichiers
    if (file_write == NULL){
        printf("Problème création fichier décrypté\n");
        exit(EXIT_FAILURE);
    }
    if (file_read == NULL){
        printf("Problème ouverture fichier crypté\n");
        exit(EXIT_FAILURE);
    }

    // Si il n'y a pas de fichier peroq.def, on demande le perroquet
    if (perroquet=='n'){
        printf("Quel est le perroquet pour décrypter? \n");
        fflush(stdin);
        fgets(per, TAILLE_MAX, stdin);
        per[strcspn(per, "\n")] = 0;
    }
    // Si il y a un fichier peroq.def
    else{
        strcpy(per, read_perroquet("peroq.def"));
    }

    // On récupère le caractère
    c = fgetc(file_read);
    // On parcourt le fichier crypté
    while (!feof(file_read)){
        // Si le code ASCII est de 127, on met la lettre du perroquet
        if ((int)c == 127){
            fputc(per[indice], file_write);
        }
        // Sinon on rajoute le perroquet
        else
            fputc(c+per[indice], file_write);

        indice++;
        if (indice == strlen(per)) // si on dépasse la longueur du perroquet on revient à 0
            indice=0;
        // caractère suivant
        c = fgetc(file_read);
    }

    fclose(file_write);
    fclose(file_read);
}
