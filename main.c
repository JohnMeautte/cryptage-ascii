#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "./header/main.h"

#define TAILLE_MAX 1000




int main(int argc, char const *argv[])
{
    char *source_filename = "source.txt";
    char *perroquet_filename = "peroq.def";
    char **tab_source = NULL;
    char perroquet[TAILLE_MAX] = "";
    int nb_lines = 0, choix = 0;
    char perroquet_choix;

    do{
        // Affichage menu
        menu(&choix);

        // Si sortie du programme
        if (choix == 0){
            printf("SORTIE DU PROGRAMME\n");
            free_tab(tab_source, nb_lines);
        }
        // Si encrypte source.txt
        else if (choix == 1){
            tab_source = read_source(source_filename, tab_source, &nb_lines);

            verify_perroquet(&perroquet_choix);
            if (perroquet_choix=='o')
                strcpy(perroquet, read_perroquet(perroquet_filename));
            else{
                printf("DONNER UN PERROQUET\n");
                fflush(stdin);
                fgets(perroquet, TAILLE_MAX, stdin);
                perroquet[strcspn(perroquet, "\n")] = 0;
            }
            create_encrypt_file(tab_source, perroquet, nb_lines);
        }
        // Si decrypte le fichier crypté
        else if (choix == 2){
            verify_perroquet(&perroquet_choix);
            decrypt(perroquet_choix);
        }
        // Si réinitialise pour refaire la démarche 1->2
        else if (choix == 3){
            free_tab(tab_source, nb_lines);
            nb_lines = 0;
            tab_source = NULL;
        }
        else if (choix == 4){
            show_help();
        }
    }while(choix != 0); 



    return 0;
}

