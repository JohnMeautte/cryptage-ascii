# Cryptage ASCII

## INTRODUCTION

Ce programme écrit en C va encrypter un fichier texte contenant des phrases que l'on veut garder secrètes. Pour cela, il va utiliser l'algorithme du "perroquet". Il va soustraire le code ASCII de chaque lettre de notre fichier texte que l'on veut encrypter par le code ASCII de chaque lettre d'un mot (ou d'une phrase) appelé(e) "perroquet" que l'on donnera soit via un fichier soit directement en ligne de commande. Voici un exemple ci-dessous:

![image info](./imgs/Capture1.png)

## Structure des fichiers
```bash
cryptage-ascii/
├── README.md
├── func
│   ├── crypt.c
│   ├── files.c
│   └── other.c
├── header
│   └── main.h
├── imgs
│   └── Capture1.png
└── main.c
```


- Le dossier func contient toutes les fonctions .c nécessaires au fonctionnement du programme
- Le dossier header contient le fichier .h qui relie les fonctions au fichier main.c
- Le dossier imgs contient les images pour l'illustration dans le README.md
- le fichier main.c contient la fonction main permettant de faire fonctionner le programme

## Fonctionnement

Le programme doit être compilé. 
voici la commande pour se faire:

```bash
gcc -o crypt main.c func/*
```
Il suffit d'éxecuter le programme comme ceci en s'assurant que les droits sont accordés pour le faire:

```bash
./crypt
```

Pour que le programme fonctionne, il faut impérativement un fichier source.txt qui contient le texte que vous voulez encrypter.
un fichier peroq.def est facultatif si vous voulez rentrer le perroquet en ligne de commande, sinon il vous faudra le créer avec un mot ou une phrase sur une seule ligne. Enfin lorsque vous utilisez l'option permettant de décrypter le texte crypté qui sera stocké dans le fichier dest.crt, ce texte decrypté sera noté dans le fichier texte_decrypte.txt.

