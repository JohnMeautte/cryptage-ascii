#ifndef HEADER_MAIN_
#define HEADER_MAIN_

char **read_source(char *filename, char **tab, int *nb_lines);
const char *read_perroquet(char *filename);
void create_encrypt_file(char **tab, char *perroquet, int nb_lines);
char **encrypt(char **tab, char *perroquet, int nb_lines);
void decrypt(char perroquet);
void free_tab(char **tab, int nb_lines);
void menu(int *choix);
void verify_perroquet(char *perroquet);
void show_help(void);

#endif
